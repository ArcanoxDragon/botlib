﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotLib
{
    public class Color
    {

        public Color(int r, int g, int b, double a)
        {
            this.R = r;
            this.G = g;
            this.B = b;
            this.A = a;
        }

        public Color(int r, int g, int b) : this(r, g, b, 1.0) { }

        public int R { get; set; }
        public int G { get; set; }
        public int B { get; set; }
        public double A { get; set; }

        public override string ToString()
        {
            return string.Format("rgba({0}, {1}, {2}, {3})", R, G, B, A);
        }
    }
}
