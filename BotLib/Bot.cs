﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace BotLib
{
    public class Bot
    {
        private string _status;
        private double _progress;
        private BotSocket socket;
        private Color color;
        private int drawSpacing;
        private Rectangle canvas;
        private Font font;
        private bool connected;

        public delegate void StatusUpdatedHandler(Bot sender, string message);
        public delegate void ProgressUpdatedHandler(Bot sender, double progress);
        public delegate void CanvasClearHandler();
        public delegate void SegmentDrawnHandler(double sX, double sY, double eX, double eY, Color color);

        public event StatusUpdatedHandler OnStatusUpdated;
        public event ProgressUpdatedHandler OnProgressUpdated;
        public event CanvasClearHandler OnCanvasClear;
        public event SegmentDrawnHandler OnSegmentDrawn;
        
        public Bot(string server, Rectangle canvas)
        {
            this._status = "";
            this._progress = 0.0;
            this.socket = new BotSocket(this, server);
            this.canvas = canvas;
            this.font = null;
            this.color = new Color(0, 0, 0);
            this.drawSpacing = 1;
            this.connected = false;
        }

        public void Connect()
        {
            Status = "Connecting to server...";
            socket.OnCanvasClear += new CanvasClearHandler(socket_OnCanvasClear);
            socket.OnSegmentDrawn += new SegmentDrawnHandler(socket_OnSegmentDrawn);
            connected = socket.Init();
            if (connected)
                Status = "Connected.";
            else
                Status = "Could not connect to server.";
        }

        void socket_OnCanvasClear()
        {
            if (OnCanvasClear != null)
                OnCanvasClear();
        }

        void socket_OnSegmentDrawn(double sX, double sY, double eX, double eY, Color color)
        {
            if (OnSegmentDrawn != null)
                OnSegmentDrawn(sX, sY, eX, eY, color);
        }

        /// <summary>
        /// This method attempts to authenticate with the bot server using the given password.
        /// If the bot does not respond immediately it will lock the thread for up to 2.5 seconds.
        /// </summary>
        /// <param name="password"></param>
        /// <returns>Whether the authentication was successful or not</returns>
        public bool Authenticate(string password)
        {
            Status = "Authenticating with server...";
            bool auth = socket.Authenticate(password);
            if (auth)
                Status = "Authenticated.";
            else
                Status = "Authentication failed.";
            return auth;
        }

        public bool Connected
        {
            get
            {
                return connected;
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                if (OnStatusUpdated != null)
                    OnStatusUpdated(this, _status);
            }
        }

        public double Progress
        {
            get
            {
                return _progress;
            }
            set
            {
                _progress = Math.Max(0.0, Math.Min(value, 1.0));
                if (OnProgressUpdated != null)
                    OnProgressUpdated(this, _progress);
            }
        }

        public int DrawSpacing
        {
            get
            {
                return drawSpacing;
            }
            set
            {
                drawSpacing = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public Font Font
        {
            get
            {
                return font;
            }
            set
            {
                font = value;
            }
        }

        public bool IsAdmin
        {
            get
            {
                return socket.IsAdmin;
            }
        }

        #region Drawing Functions

        private void ClipCoordinates(ref int x, ref int y)
        {
            x = Math.Min(Math.Max(x, 0), canvas.Width);
            y = Math.Min(Math.Max(y, 0), canvas.Height);
        }

        public void AdminClear()
        {
            if (IsAdmin)
            {
                socket.AdminClear();
                Status = "Cleared screen.";
            }
            else
            {
                Status = "Cannot clear screen as non-admin";
            }
        }

        public void DrawLine(int sX, int sY, int eX, int eY)
        {
            if (connected)
            {
                int tSX = sX;
                int tSY = sY;
                int tEX = eX;
                int tEY = eY;
                ClipCoordinates(ref tSX, ref tSY);
                ClipCoordinates(ref tEX, ref tEY);
                socket.SendSegment(tSX, tSY, tEX, tEY, color);
            }
        }

        public void DrawPoint(int x, int y)
        {
            if (connected)
            {
                DrawLine(x, y, x, y);
            }
        }

        public void DrawCircle(int x, int y, int r)
        {
            if (connected)
            {
                double circum = Math.PI * r * 2;
                double angle = 0;
                int lX = x + (int) ((double) r * Math.Cos(angle));
                int lY = y + (int) ((double) r * Math.Sin(angle));
                int cX, cY;
                double del = Math.PI / (circum / 16);
                for (angle = 0.0; angle <= 2.0 * Math.PI + del; angle += del)
                {
                    cX = x + (int) ((double) r * Math.Cos(angle));
                    cY = y + (int) ((double) r * Math.Sin(angle));
                    DrawLine(cX, cY, lX, lY);
                    lX = cX;
                    lY = cY;
                }
            }
        }

        public void DrawRectangle(int x, int y, int w, int h)
        {
            if (connected)
            {
                DrawLine(x, y, x + w, y); // Top
                DrawLine(x, y + h, x + w, y + h); // Bottom
                DrawLine(x, y, x, y + h); // Left
                DrawLine(x + w, y, x + w, y + h); // Right
            }
        }

        public void DrawRectangle(Rectangle rectangle)
        {
            DrawRectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }

        public void FillRectangle(int x, int y, int w, int h)
        {
            if (connected)
            {
                for (int yy = y; yy <= y + h; yy += drawSpacing)
                {
                    DrawLine(x, yy, x + w, yy);
                }
            }
        }

        public void FillRectangle(Rectangle rectangle)
        {
            FillRectangle(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }

        public void DrawCharacter(int x, int y, char c)
        {
            if (connected)
            {
                if (font != null)
                {
                    byte[] px = font[c];
                    byte cp;
                    for (int cx = 0; cx < font.Width; cx++)
                    {
                        for (int cy = 0; cy < font.Height; cy++)
                        {
                            cp = px[cy * font.Width + cx];
                            if (cp > 0)
                                DrawPoint(x + cx * drawSpacing, y + cy * drawSpacing);
                        }
                    }
                }
            }
        }

        public void DrawText(int x, int y, string text, bool background)
        {
            if (connected)
            {
                if (font != null)
                {
                    Color tCol = Color;
                    Color bCol = new Color(tCol.R / 8, tCol.G / 8, tCol.B / 8);
                    int px = x;
                    int py = y;
                    int cWid = (font.Width + 2) * drawSpacing;
                    int sWid = canvas.Width - x;
                    int wLen = sWid / cWid;
                    int curWid = text.Length;
                    int lastSpace = 0;
                    int tries = 0; // prevent infinite loop if we don't find a space
                    do
                    {
                        bool found = false;
                        for (int i = lastSpace + wLen; i < text.Length && i >= 0 && !found; i--)
                        {
                            if (text[i] == ' ' || text[i] == '\\') // we check for a manual break here to set width correctly
                            {
                                text = text.Remove(i, 1);
                                text = text.Insert(i, "\\");
                                found = true;
                                curWid -= i - lastSpace;
                                lastSpace = i;
                            }
                        }
                        tries++;
                    }
                    while (curWid > wLen && tries < text.Length / wLen + 1);
                    for (int i = 0; i < text.Length; i++)
                    {
                        if (text[i] == '\\')
                        {
                            px = x;
                            py += drawSpacing * (font.Height + 2);
                        }
                        else
                        {
                            if (background)
                            {
                                Color = bCol;
                                DrawCharacter(px, py, '|');
                                Color = tCol;
                            }
                            DrawCharacter(px, py, text[i]);
                            px += drawSpacing * (font.Width + 2);
                        }
                    }
                }
                else
                {
                    Status = "No font loaded";
                }
            }
        }

        public void DrawImage(int x, int y, Bitmap image)
        {
            if (connected)
            {
                Status = "Uploading image...";
                int totP = image.Width * image.Height;
                List<Point> drawn = new List<Point>();
                for (int iX = 0; iX < image.Width; iX++)
                {
                    for (int iY = 0; iY < image.Height; iY++)
                    {
                        if (drawn.Contains(new Point(iX, iY)))
                            continue;
                        System.Drawing.Color cur = image.GetPixel(iX, iY);
                        drawn.Add(new Point(iX, iY));
                        int tY = iY + 1;
                        while (tY < image.Height && cur.Equals(image.GetPixel(iX, tY)))
                        {
                            drawn.Add(new Point(iX, tY));
                            tY++;
                        }
                        tY--;
                        Color.R = cur.R;
                        Color.G = cur.G;
                        Color.B = cur.B;
                        Color.A = (double) cur.A / 255.0;
                        int curP = iX * image.Height + iY;
                        double dCurP = (double) curP / (double) totP;
                        Progress = dCurP;
                        DrawLine(x + iX * drawSpacing, y + iY * drawSpacing, x + iX * drawSpacing, y + tY * drawSpacing);
                    }
                }
                Progress = 0;
            }
        }

        public void DrawImage(int x, int y, string fname)
        {
            if (connected)
            {
                if (fname.Length > 0)
                {
                    try
                    {
                        Bitmap image = new Bitmap(Image.FromFile(fname));
                        if (image != null)
                            DrawImage(x, y, image);
                    }
                    catch (IOException ex)
                    {
                        Status = "Error loading image file";
                    }
                }
            }
        }

        public void SendInterBotMessage(object[] message)
        {
            socket.SendInterBotMessage(message);
        }

        #endregion
    }
}
