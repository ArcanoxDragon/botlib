﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BotLib
{
    public class Font
    {
        byte[] nullChar;
        Dictionary<char, byte[]> chars;
        int width, height;

        public Font(string filename, int width, int height)
        {
            this.nullChar = new byte[width * height];
            for (int i = 0; i < width * height; i++)
                nullChar[i] = 0;
            this.chars = new Dictionary<char, byte[]>();
            this.width = width;
            this.height = height;
            LoadFont(filename);
        }

        public byte[] this[char c]
        {
            get
            {
                if (chars.ContainsKey(c))
                    return chars[c];
                else
                    return nullChar;
            }
        }

        public int Width
        {
            get
            {
                return width;
            }
        }

        public int Height
        {
            get
            {
                return height;
            }
        }

        private void LoadFont(string filename)
        {
            chars = new Dictionary<char, byte[]>();
            try
            {
	            FileStream fs = new FileStream(filename, FileMode.Open);
                StreamReader read = new StreamReader(fs);
	            string line = "";
	            while (!read.EndOfStream)
	            {
		            line = read.ReadLine();
		            if (line.Length > 1)
		            {
			            if (line[0] != '`') //comment
			            {
				            char c = line[0];
				            if (c >= 0 && c < 256)
				            {
					            byte[] cFont = new byte[width * height];
					            int j = 0;
					            for (int i = 1; i < line.Length; i++)
					            {
						            if (line[i] == '0' || line[i] == '1')
						            {
							            if (j < width * height)
								            cFont[j] = (byte) (line[i] == '1' ? 1 : 0);
							            j++;
						            }
						            if (j < width * height)
						            {
							            for (int k = j; k < width * height; k++)
							            {
								            cFont[k] = 0;
							            }
						            }
					            }
                                chars[c] = cFont;
				            }
			            }
		            }
	            }
                fs.Close();
            }
            catch (IOException ex)
            {
                throw ex;
            }
        }
    }
}
