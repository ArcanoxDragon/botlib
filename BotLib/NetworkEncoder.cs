﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BotLib
{
    class NetworkEncoder
    {
        public static string HexEncodeRGB(Color c)
        {
            string hex = String.Format("#{0:X2}{1:X2}{2:X2}", c.R & 0xFF, c.G & 0xFF, c.B & 0xFF);
            return hex;
        }

        public static string EncodeSegment(double sX, double sY, double eX, double eY, Color c)
        {
            string data = "5:::";
            data += "{\"name\":\"drawSegment\",\"args\":[";
            data += sX + ",";
            data += sY + ",";
            data += eX + ",";
            data += eY + ",\"";
            data += HexEncodeRGB(c);
            data += "\"]}";
            return data;
        }

        public static string EncodeHeartbeat()
        {
            string data = "2:::";
            return data;
        }

        public static byte[] StringToByteArray(string str)
        {
            char[] cData = str.ToCharArray();
            byte[] bData = new byte[cData.Length];
            for (int i = 0; i < cData.Length; i++)
            {
                bData[i] = (byte) cData[i];
            }
            return bData;
        }

        public static string ByteArrayToString(byte[] arry)
        {
            string str = "";
            for (int i = 0; i < arry.Length; i++)
            {
                str += (char) arry[i];
            }
            return str;
        }
    }
}
