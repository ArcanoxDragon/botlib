﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Collections.Specialized;
using SocketIOClient;
using SocketIOClient.Messages;
using System.Threading;

namespace BotLib
{
    class BotSocket
    {
        public event Bot.CanvasClearHandler OnCanvasClear;
        public event Bot.SegmentDrawnHandler OnSegmentDrawn;

        private string url;
        private bool auth;
        private Client client;
        private Bot owner;

        public BotSocket(Bot owner, string url)
        {
            this.url = url;
            this.client = new Client(url);
            this.auth = false;
            this.owner = owner;
        }

        public bool Init()
        {
            client.Connect();
            client.SocketConnectionClosed += new EventHandler(SocketConnectionClosed);
            client.On("clear", OnClear);
            client.On("auth", OnAuthenticate);
            client.On("drawSegment", OnSegment);
            long millis = CurMillis();
            long timeout = 10 * 1000; // 10 seconds
            while (client.ReadyState != WebSocket4Net.WebSocketState.Open)
            {
                if (CurMillis() > millis + timeout)
                    return false;
            }
            return true;
        }

        public bool IsAdmin
        {
            get
            {
                return auth;
            }
        }

        void OnClear(IMessage msg)
        {
            if (OnCanvasClear != null)
                OnCanvasClear();
        }

        void OnAuthenticate(IMessage msg)
        {
            if (((bool) msg.Json.Args[0]) == true)
            {
                auth = true;
            }
            else
            {
                auth = false;
            }
        }

        void OnSegment(IMessage msg)
        {
            if (msg.Json.Args.Length >= 5)
            {
                try
                {
                    double sX = msg.Json.Args[0];
                    double sY = msg.Json.Args[1];
                    double eX = msg.Json.Args[2];
                    double eY = msg.Json.Args[3];
                    string cString = msg.Json.Args[4];
                    if (cString.Length == 7 && cString.StartsWith("#"))
                    {
                        cString = cString.Substring(1);
                        string sR = cString.Substring(0, 2);
                        string sG = cString.Substring(2, 2);
                        string sB = cString.Substring(4, 2);
                        byte R = byte.Parse(sR, System.Globalization.NumberStyles.HexNumber);
                        byte G = byte.Parse(sG, System.Globalization.NumberStyles.HexNumber);
                        byte B = byte.Parse(sB, System.Globalization.NumberStyles.HexNumber);
                        Color c = new Color(R, G, B);
                        if (OnSegmentDrawn != null)
                            OnSegmentDrawn(sX, sY, eX, eY, c);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        void SocketConnectionClosed(object sender, EventArgs e)
        {
            throw new WebException("Socket closed!");
        }

        public bool Authenticate(string password)
        {
            if (client.ReadyState == WebSocket4Net.WebSocketState.Open)
            {
                auth = false;
                string dPass = "";
                for (int i = 0; i < password.Length; i++)
                    dPass += "*";
                client.Emit("auth", new object[] { password });
                long sTime = CurMillis();
                long timeout = (long) (2.5 * 1000); // 2.5 seconds
                while (!auth && CurMillis() < sTime + timeout)
                    Thread.Sleep(100);
                return auth;
            }
            return false;
        }

        public void AdminClear()
        {
            if (client.ReadyState == WebSocket4Net.WebSocketState.Open)
            {
                client.Emit("clear", new object[] {});
            }
        }

        public void SendSegment(double sX, double sY, double eX, double eY, Color c)
        {
            if (eX == sX)
                eX += 0.001;
            if (eY == sY)
                eY += 0.001;
            object[] pars = new object[5];
            pars[0] = sX;
            pars[1] = sY;
            pars[2] = eX;
            pars[3] = eY;
            pars[4] = c.ToString();
            if (client.ReadyState == WebSocket4Net.WebSocketState.Open)
            {
                client.Emit("drawSegment", pars);
            }
        }

        public void SendInterBotMessage(object[] message)
        {
            object[] pars = new object[5];
            pars[0] = -1;
            pars[1] = -1;
            pars[2] = -1;
            pars[3] = -1;
            pars[4] = message;
            if (client.ReadyState == WebSocket4Net.WebSocketState.Open)
            {
                client.Emit("drawSegment", pars);
            }
        }

        private static long CurMillis()
        {
            DateTime origin = new DateTime(1970, 1, 1);
            DateTime now = DateTime.Now;
            TimeSpan since = now - origin;
            return (long) since.TotalMilliseconds;
        }
    }
}
