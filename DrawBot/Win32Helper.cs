﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;

namespace DrawBot
{
    class Win32Helper
    {
        [DllImport("USER32.dll")]
        public static extern short GetKeyState(int key);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetCursorPos(out POINT lpPoint);

        public struct POINT
        {
            public int X;
            public int Y;

            public POINT(int X, int Y)
            {
                this.X = X;
                this.Y = Y;
            }
        }

        public static bool IsKeyDown(ConsoleKey key)
        {
            int keyVal = (int) key;
            return (GetKeyState(keyVal) & 0x80) != 0;
        }

        public static void WaitForKey(ConsoleKey key)
        {
            while (IsKeyDown(key)) Thread.Sleep(1); // don't respond if already pressed
            while (!IsKeyDown(key)) Thread.Sleep(1); // wait for press
            while (IsKeyDown(key)) Thread.Sleep(1); // wait for release
        }

        public static bool WaitForKey(ConsoleKey key, int timeoutMillis)
        {
            long sMills = DateTime.Now.Millisecond;
            while (IsKeyDown(key))
            {
                if (DateTime.Now.Millisecond > sMills + timeoutMillis)
                    return false;
            }
            while (!IsKeyDown(key))
            {
                if (DateTime.Now.Millisecond > sMills + timeoutMillis)
                    return false;
            }
            while (IsKeyDown(key))
            {
                if (DateTime.Now.Millisecond > sMills + timeoutMillis)
                    return false;
            }
            return true;
        }

        public static POINT GetCursorPos()
        {
            POINT p = new POINT(0, 0);
            GetCursorPos(out p);
            return p;
        }
    }
}
