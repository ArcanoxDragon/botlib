﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace DrawBot
{
    class Settings
    {
        private static XmlDocument doc;

        private static string XMLTemplate =
@"<settings>
    <server url=""http://doodle.mast3rplan.me/"" password="""" />
    <canvas x=""0"" y=""0"" width=""1280"" height=""720"" />
    <brush width=""4"" />
	<font width=""5"" height=""7"" file=""font.txt"" />
</settings>";

        static Settings()
        {
            try
            {
                if (!File.Exists("settings.xml"))
                {
                    StreamWriter defWriter = File.CreateText("settings.xml");
                    defWriter.WriteLine(XMLTemplate);
                    defWriter.Close();
                }
                doc = new XmlDocument();
                doc.Load("settings.xml");
            }
            catch (Exception e)
            {
                throw new ApplicationException("Error loading configuration file!", e);
            }
            if (doc == null)
            {
                throw new ApplicationException("Error initializing configuration file!");
            }
        }

        public static string ServerURL
        {
            get
            {
                return doc.SelectSingleNode("/settings/server").Attributes["url"].Value;
            }
        }

        public static string Password
        {
            get
            {
                return doc.SelectSingleNode("/settings/server").Attributes["password"].Value;
            }
        }

        public static string FontFile
        {
            get
            {
                return doc.SelectSingleNode("/settings/font").Attributes["file"].Value;
            }
        }

        public static int BrushWidth
        {
            get
            {
                return int.Parse(doc.SelectSingleNode("/settings/brush").Attributes["width"].Value);
            }
        }

        public static int FontWidth
        {
            get
            {
                return int.Parse(doc.SelectSingleNode("/settings/font").Attributes["width"].Value);
            }
        }

        public static int FontHeight
        {
            get
            {
                return int.Parse(doc.SelectSingleNode("/settings/font").Attributes["height"].Value);
            }
        }

        public static int CanvasX
        {
            get
            {
                return int.Parse(doc.SelectSingleNode("/settings/canvas").Attributes["x"].Value);
            }
            set
            {
                doc.SelectSingleNode("/settings/canvas").Attributes["x"].Value = value.ToString();
                XmlWriter writer = XmlWriter.Create("settings.xml");
                doc.Save(writer);
                writer.Close();
            }
        }

        public static int CanvasY
        {
            get
            {
                return int.Parse(doc.SelectSingleNode("/settings/canvas").Attributes["y"].Value);
            }
            set
            {
                doc.SelectSingleNode("/settings/canvas").Attributes["y"].Value = value.ToString();
                XmlWriter writer = XmlWriter.Create("settings.xml");
                doc.Save(writer);
                writer.Close();
            }
        }

        public static int CanvasW
        {
            get
            {
                return int.Parse(doc.SelectSingleNode("/settings/canvas").Attributes["width"].Value);
            }
            set
            {
                doc.SelectSingleNode("/settings/canvas").Attributes["width"].Value = value.ToString();
                XmlWriter writer = XmlWriter.Create("settings.xml");
                doc.Save(writer);
                writer.Close();
            }
        }

        public static int CanvasH
        {
            get
            {
                return int.Parse(doc.SelectSingleNode("/settings/canvas").Attributes["height"].Value);
            }
            set
            {
                doc.SelectSingleNode("/settings/canvas").Attributes["height"].Value = value.ToString();
                XmlWriter writer = XmlWriter.Create("settings.xml");
                doc.Save(writer);
                writer.Close();
            }
        }
    }

}
