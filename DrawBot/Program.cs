﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using BotLib;
using System.Threading;

namespace DrawBot
{
    class Program
    {
        #region Terminal interruption
        [DllImport("User32.Dll", EntryPoint = "PostMessageA")]
        private static extern bool PostMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

        const int VK_RETURN = 0x0D;
        const int WM_KEYDOWN = 0x100;
        #endregion

        private static string statBarTxt = "";
        private static double curProgress = 0.0;
        private static bool running;
        private static Exception termExep = null;

        #region Util
        public static long CurMillis()
        {
            DateTime origin = new DateTime(1970, 1, 1);
            DateTime now = DateTime.Now;
            TimeSpan since = now - origin;
            return (long) since.TotalMilliseconds;
        }

        public static void PrintException(Exception e)
        {
            Console.WriteLine(e.Message);
            Console.WriteLine(e.StackTrace);
            foreach (string s in e.Data)
            {
                Console.WriteLine(s);
            }
            if (e.InnerException != null)
            {
                Console.Write("Caused by: ");
                PrintException(e.InnerException);
            }
        }

        public static void SetStatusBarText(string text)
        {
            statBarTxt = text;
            int cX, cY;
            ConsoleColor fG, bG;
            cX = Console.CursorLeft;
            cY = Console.CursorTop;
            fG = Console.ForegroundColor;
            bG = Console.BackgroundColor;
            Console.CursorLeft = 0;
            Console.CursorTop = Console.WindowHeight - 1;
            Console.BackgroundColor = fG;
            Console.ForegroundColor = bG;
            for (int i = 0; i < Console.WindowWidth; i++)
            {
                Console.Write(" ");
            }
            Console.CursorLeft = 0;
            Console.CursorTop = Console.WindowHeight - 1;
            Console.Write(text);
            Console.CursorLeft = 0;
            Console.CursorTop = 0;
            Console.CursorLeft = cX;
            Console.CursorTop = cY;
            Console.ForegroundColor = fG;
            Console.BackgroundColor = bG;
        }

        public static void SetProgressBar(double progress)
        {
            curProgress = progress;
            int cX, cY;
            ConsoleColor fG, bG;
            cX = Console.CursorLeft;
            cY = Console.CursorTop;
            fG = Console.ForegroundColor;
            bG = Console.BackgroundColor;
            Console.CursorLeft = 0;
            Console.CursorTop = Console.WindowHeight - 2;
            Console.BackgroundColor = fG;
            Console.ForegroundColor = bG;
            int pWid = (int) (progress * (double) Console.WindowWidth);
            for (int i = 0; i < pWid; i++)
            {
                Console.Write(" ");
            }
            Console.CursorLeft = 0;
            Console.CursorTop = Console.WindowHeight - 2;
            Console.CursorLeft = 0;
            Console.CursorTop = 0;
            Console.CursorLeft = cX;
            Console.CursorTop = cY;
            Console.ForegroundColor = fG;
            Console.BackgroundColor = bG;
        }

        public static void ClearConsole()
        {
            Console.Clear();
            SetStatusBarText(statBarTxt);
            SetProgressBar(curProgress);
            Console.CursorTop = 0;
            Console.CursorLeft = 0;
        }

        static void IgnoreInput()
        {
            while (Console.KeyAvailable)
            {
                Console.ReadKey(true);
            }
        }

        public static void Terminate(Exception e)
        {
            running = false;
            termExep = e;
            // Interrupt console
            var hWnd = System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle;
            PostMessage(hWnd, WM_KEYDOWN, VK_RETURN, 0);
        }

        #endregion

        static void OnStatusUpdated(Bot sender, string status)
        {
            SetStatusBarText(status);
        }

        static void OnProgressUpdated(Bot sender, double progress)
        {
            SetProgressBar(progress);
        }

        static void Main(string[] args)
        {
            Console.Title = "DrawBot";
            try
            {
                new Settings();
                Bot bot = new Bot(Settings.ServerURL, new System.Drawing.Rectangle(0, 0, Settings.CanvasW, Settings.CanvasH));
                bot.Font = new Font(Settings.FontFile, Settings.FontWidth, Settings.FontHeight);
                bot.DrawSpacing = Settings.BrushWidth;
                bot.OnStatusUpdated += new Bot.StatusUpdatedHandler(OnStatusUpdated);
                bot.OnProgressUpdated += new Bot.ProgressUpdatedHandler(OnProgressUpdated);
                running = true;
                bot.Connect();
                long sTime = CurMillis();
                long timeout = (long) (10 * 1000);
                while (!bot.Connected && CurMillis() < sTime + timeout)
                    Thread.Sleep(100);
                bool success = bot.Connected;
                if (!success)
                {
                    Console.WriteLine("Could not connect to the server.");
                }
                else
                {
                    bot.Authenticate(Settings.Password);
                    while (running)
                    {
                        ClearConsole();
                        Console.WriteLine("What would you like to do?");
                        Console.WriteLine("1. Draw rectangle");
                        Console.WriteLine("2. Draw circle");
                        Console.WriteLine("3. Draw text");
                        Console.WriteLine("\t3b. Draw text (with background)");
                        Console.WriteLine("4. Draw matrix (Not implemented)");
                        Console.WriteLine("5. Draw image");
                        Console.WriteLine("6. Clear screen");
                        Console.WriteLine("\t6b. Admin Clear");
                        Console.WriteLine("7. Set Color");
                        Console.WriteLine("\t7b. Set Draw Spacing");
                        Console.WriteLine("8. Recalibrate bounds");
                        Console.Write("> ");
                        string opt = Console.ReadLine();
                        if (opt == "1") // Rectangle
                        {
                            SetStatusBarText("Place mouse at top-left corner and press ENTER");
                            int sX, sY;
                            int eX, eY;
                            Win32Helper.POINT cPos;
                            Win32Helper.WaitForKey(ConsoleKey.Enter);
                            cPos = Win32Helper.GetCursorPos();
                            sX = cPos.X;
                            sY = cPos.Y;
                            SetStatusBarText("Place mouse at bottom-right corner and press ENTER");
                            Win32Helper.WaitForKey(ConsoleKey.Enter);
                            cPos = Win32Helper.GetCursorPos();
                            eX = cPos.X;
                            eY = cPos.Y;
                            sX -= Settings.CanvasX;
                            sY -= Settings.CanvasY;
                            eX -= Settings.CanvasX;
                            eY -= Settings.CanvasY;
                            SetStatusBarText(String.Format("Drawing a rectangle between ({0}, {1}) and ({2}, {3})...", sX, sY, eX, eY));
                            bot.DrawRectangle(sX, sY, eX - sX, eY - sY);
                            IgnoreInput();
                        }
                        else if (opt == "2") // Draw circle
                        {
                            SetStatusBarText("Place mouse at middle and press ENTER");
                            int sX, sY;
                            int eX, eY;
                            Win32Helper.POINT cPos;
                            Win32Helper.WaitForKey(ConsoleKey.Enter);
                            cPos = Win32Helper.GetCursorPos();
                            sX = cPos.X;
                            sY = cPos.Y;
                            SetStatusBarText("Place mouse at radius and press ENTER");
                            Win32Helper.WaitForKey(ConsoleKey.Enter);
                            cPos = Win32Helper.GetCursorPos();
                            eX = cPos.X;
                            eY = cPos.Y;
                            sX -= Settings.CanvasX;
                            sY -= Settings.CanvasY;
                            eX -= Settings.CanvasX;
                            eY -= Settings.CanvasY;
                            int radius = (int) Math.Sqrt(Math.Pow(eX - sX, 2) + Math.Pow(eY - sY, 2));
                            SetStatusBarText(String.Format("Drawing a circle at ({0}, {1}) with radius {2}...", sX, sY, radius));
                            bot.DrawCircle(sX, sY, radius);
                            IgnoreInput();
                        }
                        else if (opt == "3") // Text
                        {
                            Console.Write("Text: ");
                            string text = Console.ReadLine();
                            SetStatusBarText("Place mouse at location to draw text and press ENTER");
                            Win32Helper.POINT cPos;
                            Win32Helper.WaitForKey(ConsoleKey.Enter);
                            cPos = Win32Helper.GetCursorPos();
                            cPos.X -= Settings.CanvasX;
                            cPos.Y -= Settings.CanvasY;
                            bot.DrawText(cPos.X, cPos.Y, text, false);
                        }
                        else if (opt == "3b") // Text with background
                        {
                            Console.Write("Text: ");
                            string text = Console.ReadLine();
                            SetStatusBarText("Place mouse at location to draw text and press ENTER");
                            Win32Helper.POINT cPos;
                            Win32Helper.WaitForKey(ConsoleKey.Enter);
                            cPos = Win32Helper.GetCursorPos();
                            cPos.X -= Settings.CanvasX;
                            cPos.Y -= Settings.CanvasY;
                            bot.DrawText(cPos.X, cPos.Y, text, true);
                        }
                        else if (opt == "5")
                        {
                            Console.Write("Image: ");
                            string filename = Console.ReadLine();
                            SetStatusBarText("Place mouse at location to draw image and press ENTER");
                            Win32Helper.POINT cPos;
                            Win32Helper.WaitForKey(ConsoleKey.Enter);
                            cPos = Win32Helper.GetCursorPos();
                            cPos.X -= Settings.CanvasX;
                            cPos.Y -= Settings.CanvasY;
                            bot.DrawImage(cPos.X, cPos.Y, filename);
                            SetStatusBarText("Image uploaded");
                        }
                        else if (opt == "6") // Clear
                        {
                            bot.FillRectangle(0, 0, Settings.CanvasW, Settings.CanvasH);
                            bot.SendInterBotMessage(new object[] { "CLRSCR2COL", bot.Color.ToString() });
                            SetStatusBarText("Screen cleared");
                        }
                        else if (opt == "6b") // Admin clear
                        {
                            if (bot.IsAdmin)
                            {
                                bot.AdminClear();
                                SetStatusBarText("Screen cleared");
                            }
                            else
                            {
                                SetStatusBarText("You must be an admin to use that feature.");
                            }
                        }
                        else if (opt == "7") // Set color
                        {
                            Console.WriteLine("Enter the color values:");
                            Console.WriteLine("R: ");
                            Console.WriteLine("G: ");
                            Console.WriteLine("B: ");
                            Console.CursorLeft = 3;
                            Console.CursorTop -= 3;
                            int r, g, b;
                            bool good = true;
                            if (!int.TryParse(Console.ReadLine(), out r))
                            {
                                SetStatusBarText("Invalid number value for R");
                                good = false;
                            }
                            Console.CursorLeft = 3;
                            if (!int.TryParse(Console.ReadLine(), out g))
                            {
                                SetStatusBarText("Invalid number value for G");
                                good = false;
                            }
                            Console.CursorLeft = 3;
                            if (!int.TryParse(Console.ReadLine(), out b))
                            {
                                SetStatusBarText("Invalid number value for B");
                                good = false;
                            }
                            if (good)
                            {
                                bot.Color.R = r;
                                bot.Color.G = g;
                                bot.Color.B = b;
                                SetStatusBarText("Color set to " + bot.Color.ToString());
                            }
                        }
                        else if (opt == "7b") // Set draw spacing
                        {
                            Console.WriteLine("Enter the draw spacing:");
                            Console.Write("Spacing: ");
                            int width;
                            if (!int.TryParse(Console.ReadLine(), out width))
                            {
                                SetStatusBarText("Invalid number value for Spacing");
                            }
                            else
                            {
                                bot.DrawSpacing = width;
                                SetStatusBarText("Draw spacing set to " + width);
                            }
                        }
                        else if (opt == "8") // Recalibrate
                        {
                            SetStatusBarText("Place mouse at top-left corner and press ENTER");
                            int sX, sY;
                            int eX, eY;
                            Win32Helper.POINT cPos;
                            Win32Helper.WaitForKey(ConsoleKey.Enter);
                            cPos = Win32Helper.GetCursorPos();
                            sX = cPos.X;
                            sY = cPos.Y;
                            SetStatusBarText("Place mouse at bottom-right corner and press ENTER");
                            Win32Helper.WaitForKey(ConsoleKey.Enter);
                            cPos = Win32Helper.GetCursorPos();
                            eX = cPos.X;
                            eY = cPos.Y;
                            int w, h;
                            w = eX - sX;
                            h = eY - sY;
                            Settings.CanvasX = sX;
                            Settings.CanvasY = sY;
                            Settings.CanvasW = w;
                            Settings.CanvasH = h;
                            SetStatusBarText(String.Format("Canvas bounds set to ({0}, {1}, {2}, {3}).", sX, sY, w, h));
                            IgnoreInput();
                        }
                        else if (opt.ToLower() == "exit")
                        {
                            running = false;
                        }
                        else
                        {
                            if (opt != "")
                            {
                                SetStatusBarText("Unknown option \"" + opt + "\".");
                            }
                        }
                    }
                    if (termExep != null)
                        throw new Exception("Unhandled exception", termExep);
                }
            }
            catch (Exception ex)
            {
                Console.Write("Error: ");
                PrintException(ex);
                Console.WriteLine("The program cannot continue execution. Press any key to exit.");
                IgnoreInput();
                Console.ReadKey();
            }
        }
    }
}
